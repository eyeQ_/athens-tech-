
import java.time.LocalDate;
        import java.time.Period;
        import java.util.Scanner;

public class HeartRates {

    private String firstname;
    private String lastname;
    private int month;
    private int day;
    private int year;

    /** constructor*/

    public HeartRates() {
    }

    public HeartRates(String firstname, String lastname, int month, int day, int year) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.month = month;
        this.day = day;
        this.year = year;
    }

    /** getter's */

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }
    /** Setter's*/

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setYear(int year) {
        this.year = year;
    }

    /** inputs of the  variables from the user*/

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("enter your first name:");
        String fn = input.nextLine();
        System.out.println("enter your last name:");
        String ln = input.nextLine();
        System.out.print("Please enter date of birth in YYYY-MM-DD: ");
        String in = input.nextLine();
        LocalDate dob = LocalDate.parse(in);
        System.out.println("Age is:" + getAge(dob));
        System.out.println ("Maximum Hart Rate is :" + CalculateMaxHeartRate(dob));
        System.out.println("Target Heart Rate is:" + TargetHeartRate(dob));
    }

    public static int getAge(LocalDate dob) /**calculates the age in years of the user*/
    {
        LocalDate curDate = LocalDate.now();
        return Period.between(dob, curDate).getYears();

    }

    public static int CalculateMaxHeartRate( LocalDate dob) /** calculates the max heart rate of the user according of his current age*/
    {
        LocalDate curDate = LocalDate.now();
        int maxHeartRate = 220 -getAge(dob);
        return maxHeartRate;

    }


    public static String TargetHeartRate(LocalDate dob)
    {

        int targetHeartRateMin = (int)((50f/100f) * CalculateMaxHeartRate(dob));
        int targetHeartRateMax = (int)((85f/100f) * CalculateMaxHeartRate(dob));
        String targetHeartRate = " between " + targetHeartRateMin + " and " + targetHeartRateMax ;
        return targetHeartRate;
    }


    @Override
    public String toString() {
        return "HeartRates{" +
                "month=" + month +
                ", day=" + day +
                ", year=" + year +
                '}';
    }

}